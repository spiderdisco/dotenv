dotenv
======

Simple `.env` file parser for node.

## Features
 - Sets node's environment variables from .env file.
 - Provides a dictionary containing variables read from the .env file.
 - Ignores `# commented` lines.
 - Ignores `export` statements, so the same .env file can be used in the shell or node.


## Usage
```
const dotenv = require('@spiderdisco/dotenv');
dotenv('path/to/.env');
```
