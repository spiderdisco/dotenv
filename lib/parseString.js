const parseLine = require('./parseLine');
const { DotenvError, DOTENV_ERR_DUPLICATE_KEY } = require('./error');

function parseString(str) {
  if(str === undefined) {
    return {};
  }

  const result = {};
  const lines = str.split(/[\r\n]+/g);


  for(let i=0; i<lines.length; ++i) {
    const line = lines[i];
    let kv;
    try {
      kv = parseLine(line);
    } catch(err) {
      // Add line number to DotenvErrors.
      if(err instanceof DotenvError) {
        err.lineNumber = i + 1;
      }
      // Re-throw error.
      throw err;
    }
    // Skip falsy lines
    if(!kv) {
      continue;
    }

    const key = kv[0];

    // Duplicate keys are an error.
    if(key in result) {
      const err = new DotenvError(DOTENV_ERR_DUPLICATE_KEY, i+1);
      err.duplicateKey=key;
      throw err;
    }

    // Add to result
    result[key] = kv[1];
  }

  return result;
}

module.exports = parseString;
