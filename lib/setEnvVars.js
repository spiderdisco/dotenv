function setEnvVars(envVars) {
  process.env = { ...process.env, ...envVars };
}

module.exports = setEnvVars;
