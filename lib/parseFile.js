const fs = require('fs');
const parseString = require('./parseString');
const { DotenvError, DOTENV_ERR_NO_SUCH_FILE } = require('./error');

function parseFile(path) {
  let input;
  try {
    input = fs.readFileSync(path, "utf8");
  } catch(err) {
    if(err.code && err.code === 'ENOENT') {
      err = new DotenvError(DOTENV_ERR_NO_SUCH_FILE);
      err.noSuchFile = path;
    }
    throw err;
  }
  return parseString(input);
}

module.exports = parseFile;
