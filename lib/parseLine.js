const { DotenvError, DOTENV_ERR_MULTIPLE_EQ, DOTENV_ERR_NO_EQ } = require('./error');

function parseLine(line) {
  line = line.trim();

  // Skip comments & empty lines.
  if(line.length === 0 || line[0] === '#') {
    return false;
  }

  // Ignore export statements.
  if(line.startsWith('export ')) {
    line = line.substring(7);
  }

  const kv = line.split('=');

  // Multiple or zero '=' tokens is an error.
  if(kv.length !== 2) {
    if(kv.length > 2) {
      throw new DotenvError(DOTENV_ERR_MULTIPLE_EQ);
    } else {
      throw new DotenvError(DOTENV_ERR_NO_EQ);
    }
  }

  // Strip single & double quotes from values.
  if(kv[1].match(/"|'[^"]+"|'/)) {
    kv[1] =  kv[1].substring(1, kv[1].length - 1);
  }



  return kv;
}

module.exports = parseLine;
