const parseFile = require('./parseFile');
const setEnvVars = require('./setEnvVars');

function parse(path) {
  const result = parseFile(path);
  setEnvVars(result);
  return result;
}

module.exports = parse;
