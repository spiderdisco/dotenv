class DotenvError extends Error {
  constructor(errorCode, lineNumber=undefined) {
    super();
    this.name="DotenvError";
    this.errorCode = errorCode;
    this.lineNumber = lineNumber;
  }
}

module.exports = {
  DotenvError,

  DOTENV_ERR_MULTIPLE_EQ:   0,
  DOTENV_ERR_NO_EQ:         1,
  DOTENV_ERR_DUPLICATE_KEY: 2,
  DOTENV_ERR_NO_SUCH_FILE:  3,
}
