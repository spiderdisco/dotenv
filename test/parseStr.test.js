const expect = require('chai').expect;
const parseString = require('../lib/parseString');
const err = require('../lib/error');

describe('parseString()', () => {
  it('returns object like { keys...: values... }', () => {
    expect(parseString('key1=value1\nkey2=value2'))
      .to.deep.eq({key1: 'value1', key2: 'value2'});
  });

  it('returns {} for empty string', () => {
    expect(parseString(''))
      .to.deep.eq({});
  });

  it('returns {} for undefined argument', () => {
    expect(parseString())
      .to.deep.eq({});
  })

  it('adds lineNumber to MultipleEquals errors thrown in parseLine', () => {

    const invalidString = 'KEY=VALUE\nKEY2=VALUE=ERROR';

    expect(() => parseString(invalidString), 'Maintain error code.')
      .to.throw(err.DotenvError)
      .with.property('errorCode', err.DOTENV_ERR_MULTIPLE_EQ);

    expect(() => parseString(invalidString))
      .to.throw(err.DotenvError)
      .with.property('lineNumber', 2);
  });

  it('adds lineNumber to NoEquals errors thrown in parseLine', () => {
    const invalidString = 'KEY=VALUE\nKEY2VALUEERROR';

    expect(() => parseString(invalidString), 'Maintain error code.')
      .to.throw(err.DotenvError)
      .with.property('errorCode', err.DOTENV_ERR_NO_EQ);

    expect(() => parseString(invalidString))
      .to.throw(err.DotenvError)
      .with.property('lineNumber', 2);
  });

  it('throws error for duplicate keys', () => {
    const invalidString = 'KEY=VALUE\nKEY=VALUE';

    expect(() => parseString(invalidString))
    .to.throw(err.DotenvError)
    .with.property('errorCode', err.DOTENV_ERR_DUPLICATE_KEY);

    expect(() => parseString(invalidString))
    .to.throw(err.DotenvError)
    .with.property('lineNumber', 2);

    expect(() => parseString(invalidString))
    .to.throw(err.DotenvError)
    .with.property('duplicateKey', 'KEY');
  })
});
