const path = require('path');
const expect = require('chai').expect;
const parse = require('../lib/index');

describe('parse()', () => {
  const testEnvPath = path.join(__dirname, 'test.env');

  it('returns { keys...: values... }', () => {
    expect(parse(testEnvPath))
    .to.deep.equal({ KEY: 'VALUE', KEY2: 'VALUE' })
  });

  it('sets env vars', () => {
    parse(testEnvPath);

    expect(process.env.KEY).to.eq('VALUE');
    expect(process.env.KEY2).to.eq('VALUE');
  })
})
