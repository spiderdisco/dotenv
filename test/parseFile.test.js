const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-as-promised'));
const path = require('path');
const parseFile = require('../lib/parseFile');
const err = require('../lib/error');

const testEnvPath = path.join(__dirname, 'test.env');

describe('parseFile()', () => {
  it('returns {key...: values...} from file', () =>
    expect(parseFile(testEnvPath))
    .to.deep.equal({ KEY: 'VALUE', KEY2: 'VALUE' })
  );

  it('throws error for non-existant file', () => {
    const noSuchFile = './no/such/file.env';

    expect(() => parseFile(noSuchFile))
    .to.throw(err.DotenvError)
    .with.property('errorCode', err.DOTENV_ERR_NO_SUCH_FILE)
    ;

    expect(() => parseFile(noSuchFile))
    .to.throw(err.DotenvError)
    .with.property('noSuchFile', noSuchFile)

    expect(() => parseFile(noSuchFile))
    .to.throw(err.DotenvError)
    .with.property('lineNumber', undefined)
  })
});
