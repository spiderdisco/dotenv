'use strict';
const expect = require('chai').expect;
const parseLine = require('../lib/parseLine');
const err = require('../lib/error.js');


describe('parseLine()', () => {
  it('returns false for strings beginning with #', () => {
    expect(parseLine('# comment')).to.be.false;
  });

  it('returns false for empty string', () => {
    expect(parseLine('')).to.be.false;
  });

  it('returns [key, value] for strings like "key=value"', () => {
    expect(parseLine('KEY=VALUE'))
      .to.deep.eq(['KEY', 'VALUE']);
  });

  it('ignores "export " statements', () => {
    expect(parseLine('export KEY=VALUE'))
      .to.deep.eq(['KEY', 'VALUE']);
  })

  it('strips double quotes from value', () => {
    expect(parseLine(`KEY="VALUE"`))
      .to.deep.eq(['KEY', 'VALUE']);
  });

  it('strips single quotes from value', () => {
    expect(parseLine(`KEY='VALUE'`))
      .to.deep.eq(['KEY', 'VALUE']);
  });

  it('throws DotenvError if more than one "=" character found', () => {
    expect(() => parseLine('KEY=VALUE=ERROR'))
      .to.throw(err.DotenvError)
      .with.property('errorCode', err.DOTENV_ERR_MULTIPLE_EQ);
  });

  it('throws DotenvError if no "=" characters found', () => {
    expect(() => parseLine('KEYVALUE'))
    .to.throw(err.DotenvError)
    .with.property('errorCode', err.DOTENV_ERR_NO_EQ);
});
});
