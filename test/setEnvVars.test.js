const expect = require('chai').expect;
const setEnvVars = require('../lib/setEnvVars');

describe('setEnvVars()', () => {
  it('sets env vars...', () => {
    setEnvVars({key: 'value', key2: 'value'});

    expect(process.env.key).to.eq('value');
    expect(process.env.key2).to.eq('value');
  });
})
